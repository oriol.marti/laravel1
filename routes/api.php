<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/tareas', 'App\Http\Controllers\TareasController@index');

Route::put('/tareas/actualizar/{id}', 'App\Http\Controllers\TareasController@update');

Route::post('/tareas/guardar', 'App\Http\Controllers\TareasController@store');

Route::delete('/tareas/borrar/{id}', 'App\Http\Controllers\TareasController@destroy');

Route::get('/tareas/buscar/{id}', 'App\Http\Controllers\TareasController@show');
